package com.j25.cloud.bookservice.controller;

import com.j25.cloud.bookservice.BookServiceApplication;
import com.j25.cloud.bookservice.model.Book;
import com.j25.cloud.bookservice.service.BookService;
import jdk.nashorn.internal.runtime.PropertyHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;


    @GetMapping("/{id}")
    public Book getBookById(@PathVariable("id") Long id){
        Book book = bookService.find(id);
        return book;
    }

    @PostMapping()
    public Book add(@RequestBody Book book){
        return bookService.add(book);
    }

    @GetMapping("/")
    public List<Book> find(@RequestParam(name = "title", required = false) String title,
                     @RequestParam(name = "author", required = false) String author){
        List<Book> books = bookService.find(title, author);
        return books;
    }
}
