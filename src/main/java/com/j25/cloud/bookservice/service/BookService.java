package com.j25.cloud.bookservice.service;

import com.j25.cloud.bookservice.model.Book;
import com.j25.cloud.bookservice.repository.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public Book find(Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException());
    }

    public List<Book> find(String title, String author) {
        if (author != null && !author.isEmpty() && title != null && !title.isEmpty()) {
            return bookRepository.findAllByAuthorContainingOrTitleContaining(author, title);
        }
        if (author != null && !author.isEmpty()) {
            return bookRepository.findAllByAuthorContaining(author);
        }
        if (title != null && !title.isEmpty()) {
            return bookRepository.findAllByTitleContaining(title);
        }
        return new ArrayList<>();
    }

    public Book add(Book book) {
        return bookRepository.save(book);
    }
}
