package com.j25.cloud.bookservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Book {
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @JsonProperty(access = JsonProperty.Access.READ_ONLY)
   private Long id;
   @Column(nullable = false)
   private String title;
   @Column(nullable = false)
   private String author;

   private int yearWritten;
   private int numberOfPages;
   private int numberOfAvailableCopies;
}