package com.j25.cloud.bookservice.repository;

import com.j25.cloud.bookservice.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByAuthorContainingOrTitleContaining(String author, String title);
    List<Book> findAllByTitleContaining( String title);
    List<Book> findAllByAuthorContaining(String author);
}
